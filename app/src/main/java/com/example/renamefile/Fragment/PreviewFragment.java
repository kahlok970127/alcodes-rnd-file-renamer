//package com.example.renamefile.Fragment;
//
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Canvas;
//import android.graphics.Color;
//import android.graphics.Paint;
//import android.net.Uri;
//import android.os.Bundle;
//import android.provider.DocumentsContract;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.fragment.app.Fragment;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//
//import com.example.renamefile.Activity.FileActivity;
//import com.example.renamefile.Adapter.EditFileAdapter;
//import com.example.renamefile.Adapter.PreviewRenameAdapted;
//import com.example.renamefile.Model.MyUri;
//import com.example.renamefile.R;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.io.Serializable;
//import java.util.ArrayList;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import butterknife.Unbinder;
//
//public class PreviewFragment extends Fragment implements Serializable {
//    public static final String TAG = com.example.renamefile.Fragment.PreviewFragment.class.getSimpleName();
//
//    public ArrayList<MyUri> NewUriList = new ArrayList<>();
//
//    @BindView(R.id.uri_recyclerview)
//    protected RecyclerView mRecyclerView;
//
//    private PreviewRenameAdapted mAdapter;
//    private Unbinder mUnbinder;
//
//    public PreviewFragment() {
//    }
//
//    public static com.example.renamefile.Fragment.PreviewFragment newInstance() {
//        return new com.example.renamefile.Fragment.PreviewFragment();
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        setHasOptionsMenu(true);
//    }
//
//    @Override
//    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
//       // inflater.inflate(R.menu.menu_fragment_edit, menu);
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        int itemId = item.getItemId();
//
//
//        if (itemId == R.id.menu_edit) {
//           // EditFile();
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_previewrename, container, false);
//
//        Bundle bundle = this.getArguments();
//        if (bundle != null) {
//            NewUriList = (ArrayList<MyUri>) getArguments().getSerializable("key");
//        }
//
//        mUnbinder = ButterKnife.bind(this, view);
//
//
//        return view;
//    }
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        initView();
//    }
//    @OnClick(R.id.button_confirm)
//    protected void ConfirmRename() {
//
//
//        for (int i = 0; i < NewUriList.size(); i++) {
//
//            if (NewUriList.get(i).isSelect()) {//if  select then delete
//                Uri uri = Uri.parse(NewUriList.get(i).uri);
//
//                try {
//                    DocumentsContract.renameDocument(getContext().getContentResolver(), uri, NewUriList.get(i).name);
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        startActivity(new Intent(getActivity(), FileActivity.class));
//
//    }
//
//    private void initView() {
//
//
//        mAdapter = new PreviewRenameAdapted(NewUriList);
//
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
//        mRecyclerView.setHasFixedSize(true);
//        mRecyclerView.setAdapter(mAdapter);
//
//
//    }
//
//}
