package com.example.renamefile.Repository;

import android.app.Application;
import android.content.Context;
import android.net.Uri;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.renamefile.Adapter.UriAdapter;
import com.example.renamefile.Model.MyUri;
import com.example.renamefile.database.entities.MyFile;
import com.example.renamefile.database.entities.MyFileDao;
import com.example.renamefile.utility.DatabaseHelper;

import org.greenrobot.greendao.AbstractDao;

import java.util.ArrayList;
import java.util.List;

public class MyUriRepository {
    private static MyUriRepository mInstance;

    private MutableLiveData<List<UriAdapter.DataHolder>> mMyUriAdapterListLiveData = new MutableLiveData<>();
    private MutableLiveData<MyFile> mMyUriLiveData = new MutableLiveData<>();

    public static MyUriRepository getInstance() {

        if (mInstance == null) {
            synchronized (MyUriRepository.class) {
                mInstance = new MyUriRepository();
            }
        }
        return mInstance;
    }

    public LiveData<List<UriAdapter.DataHolder>> getMyUriAdapterListLiveData() {
        return mMyUriAdapterListLiveData;
    }

    public LiveData<MyFile> getMyUriLiveData() {
        return mMyUriLiveData;
    }


    public void loadMyUriAdapterList(Context context) {

        List<UriAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<MyFile> records = DatabaseHelper.getInstance(context)
                .getMyFileDao()
                .loadAll();

        if (records != null) {
            for (MyFile myFile : records) {
                UriAdapter.DataHolder dataHolder = new UriAdapter.DataHolder();
                dataHolder.id = myFile.getId();
                dataHolder.name = myFile.getName();
                dataHolder.uri=myFile.getUri();

                dataHolders.add(dataHolder);
            }
        }

        mMyUriAdapterListLiveData.setValue(dataHolders);
    }

    public void rename(Context context, String name) {


    }


    public void addFile(Context context, String fileName, String uri) {
        MyFile myFile = new MyFile();
        myFile.setName(fileName);
        myFile.setUri(uri);

        //Add record to database
        DatabaseHelper.getInstance(context)
                .getMyFileDao()
                .insert(myFile);

        loadMyUriAdapterList(context);
    }

    public void deleteFile(Context context, Long id) {
        DatabaseHelper.getInstance(context)
                .getMyFileDao()
                .deleteByKey(id);

        loadMyUriAdapterList(context);

    }

    public void editsingle(Context context, Long id, String title) {
        MyFileDao myFileDao = DatabaseHelper.getInstance(context).getMyFileDao();
        MyFile myFile = myFileDao.load(id);

        // Check if record exists.
        if (myFile != null) {
            // Record is found, now update.
            myFile.setName(title);
            myFileDao.update(myFile);

            // Done editing record, now re-load list.
            loadMyUriAdapterList(context);
        }


    }

}


