package com.example.renamefile.Model;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;


public class MyUri implements Serializable, Parcelable {

    public int id;
    public String uri;
    public transient Bitmap image;
    public String name;
    private boolean isSelect;
    public File imagefile;

    public MyUri(Parcel in) {
        id = in.readInt();
        uri = in.readString();
        name = in.readString();
        isSelect = in.readByte() != 0;
    }

    public static final Creator<MyUri> CREATOR = new Creator<MyUri>() {
        @Override
        public MyUri createFromParcel(Parcel in) {
            return new MyUri(in);
        }

        @Override
        public MyUri[] newArray(int size) {
            return new MyUri[size];
        }
    };

    public MyUri() {

    }

    public int getId() {
        return id;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public File getImagefile() {
        return imagefile;
    }

    public void setImagefile(File imagefile) {
        this.imagefile = imagefile;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(uri);
        dest.writeString(name);
        dest.writeByte((byte) (isSelect ? 1 : 0));
    }
}
