package com.example.renamefile.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.renamefile.Model.MyUri;
import com.example.renamefile.R;
import com.google.android.material.button.MaterialButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class UriAdapter extends RecyclerView.Adapter<UriAdapter.ViewHolder>  implements Serializable {
    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_uri, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_Uri_root)
        public LinearLayout root;

        @BindView(R.id.textview_name)
        public TextView title;

        @BindView(R.id.button_delete)
        public MaterialButton deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                title.setText(data.name);

                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });

                    deleteButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onDeleteButtonClicked(data);
                        }
                    });

                }

            }

        }

        public void resetViews() {
            title.setText("");
            root.setOnClickListener(null);
            deleteButton.setOnClickListener(null);
        }
    }

    public interface Callbacks {

        void onListItemClicked(DataHolder data);

        void onDeleteButtonClicked(DataHolder data);


    }


    public static class DataHolder {
        public Long id;
        public String name;
        public String uri;
    }
}