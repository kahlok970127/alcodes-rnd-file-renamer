//package com.example.renamefile.Adapter;
//
//import android.content.Context;
//import android.graphics.Color;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.bumptech.glide.Glide;
//import com.example.renamefile.Model.MyUri;
//import com.example.renamefile.R;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//
//public class EditFileAdapter extends RecyclerView.Adapter<EditFileAdapter.MyViewHolder>  implements Serializable {
//
//    public ArrayList<MyUri> newuri;
//
//    public interface ChnageStatusListener {
//        void onItemChangeListener(int position, MyUri myuri);
//    }
//
//    Context mContext;
////    UriAdapter.ChnageStatusListener chnageStatusListener;
//
//    public void setModels(ArrayList<MyUri> newuri) {
//        this.newuri = newuri;
//    }
//
////    public EditFileAdapter(ArrayList<MyUri> newuri, Context mContext, UriAdapter.ChnageStatusListener chnageStatusListener) {
////        this.newuri = newuri;
////        this.mContext = mContext;
////        this.chnageStatusListener = chnageStatusListener;
////    }
//
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//
//        public ImageView image;
//        public TextView name, uri;
//
//
//        public MyViewHolder(View view) {
//            super(view);
//            image = (ImageView) view.findViewById(R.id.imageview_image);
//
//            image.setImageResource(R.drawable.download);
//            name = (TextView) view.findViewById(R.id.textview_name);
//            uri = (TextView) view.findViewById(R.id.textview_uri);
//
//        }
//    }
//
//
//    public EditFileAdapter(ArrayList<MyUri> newuri) {
//        this.newuri = newuri;
//    }
//
//    @Override
//    public EditFileAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.item_uri, parent, false);
//
//        return new EditFileAdapter.MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(EditFileAdapter.MyViewHolder holder, int position) {
//        MyUri uri = newuri.get(position);
//        Glide.with(holder.image);
//
//        holder.name.setText(uri.name);
//
//
//        if (uri.isSelect()) {
//            holder.itemView.setBackgroundColor(Color.CYAN);
//        } else {
//            holder.itemView.setBackgroundColor(Color.WHITE);
//        }
//
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MyUri uri = newuri.get(position);
//                if (uri.isSelect()) {
//                    uri.setSelect(false);
//                    holder.itemView.setBackgroundColor(Color.CYAN);
//                } else {
//                    uri.setSelect(true);
//                    holder.itemView.setBackgroundColor(Color.WHITE);
//                }
//                newuri.set(position, uri);
////                if (chnageStatusListener != null) {
////                    chnageStatusListener.onItemChangeListener(position, uri);
////
////                }
//                notifyItemChanged(position);
//            }
//        });
//    }
//
//
//
//    @Override
//    public int getItemCount() {
//        return newuri.size();
//    }
//}