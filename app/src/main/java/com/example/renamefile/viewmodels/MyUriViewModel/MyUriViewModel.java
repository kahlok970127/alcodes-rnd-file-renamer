package com.example.renamefile.viewmodels.MyUriViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.renamefile.Adapter.UriAdapter;
import com.example.renamefile.Repository.MyUriRepository;
import com.example.renamefile.database.entities.MyFile;

import java.util.ArrayList;
import java.util.List;


public class MyUriViewModel extends AndroidViewModel {

    private MyUriRepository mMyUriRepository;

    public MyUriViewModel(@NonNull Application application) {
        super(application);
        mMyUriRepository = MyUriRepository.getInstance();
    }

    public LiveData<List<UriAdapter.DataHolder>> getMyUriAdapterListLiveData() {
        return mMyUriRepository.getMyUriAdapterListLiveData();
    }

    public LiveData<MyFile> getMyUriLiveData() {
        return mMyUriRepository.getMyUriLiveData();
    }

    public void loadMyUriAdapterList() {
        mMyUriRepository.loadMyUriAdapterList(getApplication());
    }



    public void addFile(String fileName, String uri) {
        mMyUriRepository.addFile(getApplication(), fileName, uri);
    }

    public void deleteFile(Long id) {
        mMyUriRepository.deleteFile(getApplication(), id);
    }

//    public MyFile getFile(){
//        return mMyUriRepository.getFile(getApplication());
//    }

    public void editsingle(Long id, String title) {
        mMyUriRepository.editsingle(getApplication(), id, title);
    }
}
