//package com.example.renamefile.Fragment;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.app.ProgressDialog;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.database.Cursor;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.os.Parcelable;
//import android.provider.DocumentsContract;
//import android.provider.OpenableColumns;
//import android.text.InputType;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.CheckBox;
//import android.widget.EditText;
//import android.widget.LinearLayout;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.documentfile.provider.DocumentFile;
//import androidx.fragment.app.Fragment;
//import androidx.fragment.app.FragmentTransaction;
//import androidx.lifecycle.Observer;
//import androidx.lifecycle.ViewModelProvider;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.example.renamefile.Activity.FileActivity;
//import com.example.renamefile.Activity.RenameActivity;
//import com.example.renamefile.Adapter.EditFileAdapter;
//import com.example.renamefile.Adapter.UriAdapter;
//import com.example.renamefile.Model.MyUri;
//import com.example.renamefile.R;
//import com.example.renamefile.database.entities.MyFile;
//import com.example.renamefile.viewmodels.MyUriViewModel.MyUriViewModel;
//import com.example.renamefile.viewmodels.MyUriViewModel.MyUriViewModelFactory;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.regex.Pattern;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import butterknife.Unbinder;
//
//public class RenameFragment extends Fragment implements UriAdapter.Callbacks {
//    private MyUriViewModel mViewModel;
//    private String m_Text = "";
//    @BindView(R.id.uri_recyclerview)
//    protected RecyclerView mRecyclerView;
//    private static String LIST_STATE = "list_state";
//    private Parcelable savedRecyclerLayoutState;
//    private static final String BUNDLE_RECYCLER_LAYOUT = "recycler_layout";
//    public ArrayList<MyUri> MyUriList = new ArrayList<>();
//    public ArrayList<MyUri> NewUriList = new ArrayList<>();
//    MyUri edituri = new MyUri();
//    MyUri temp = new MyUri();
//    public int option;
//    public String old_str = "";
//    public String new_str = "";
//    ProgressDialog progressDialog;
//    private Long mMyFileId = 0L;
//
//    public static final String TAG = com.example.renamefile.Fragment.RenameFragment.class.getSimpleName();
//
//
//
//    private UriAdapter mAdapter;
//    private Unbinder mUnbinder;
//
//    public RenameFragment() {
//    }
//
//    public static com.example.renamefile.Fragment.RenameFragment newInstance() {
//        return new com.example.renamefile.Fragment.RenameFragment();
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        setHasOptionsMenu(true);
//    }
//
//    @Override
//    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_fragment_edit, menu);
//
//
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        int itemId = item.getItemId();
//
//
//        if (itemId == R.id.menu_edit) {
//            EditFile();
//        } else if (itemId == R.id.menu_insertchar) {
//            InsertCharAt();
//        } else if (itemId == R.id.menu_replace) {
//            ReplaceChar();
//        } else if (itemId == R.id.menu_insertnumber) {
//            InsertNumber();
//        } else if (itemId == R.id.menu_insertfilesize) {
//            InsertFileSize();
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_rename, container, false);
//
//        Bundle bundle = this.getArguments();
//        if (bundle != null) {
//            MyUriList = (ArrayList<MyUri>) getArguments().getSerializable("key");
//        }
//
//        mUnbinder = ButterKnife.bind(this, view);
//
//
//        return view;
//    }
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        initView();
//        initViewModel();
//    }
//
//    private void initView() {
//
//
//    }
//    private List<UriAdapter.DataHolder> mData = new ArrayList<>();
//    private void initViewModel() {
//        mViewModel = new ViewModelProvider(this, new MyUriViewModelFactory(getActivity().getApplication())).get(MyUriViewModel.class);
////        mViewModel.getMyUriAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<MyFile>() {
////
////            @Override
////            public void onChanged(MyFile myFile) {
////                if (myFile != null) {
////                    = myFile.getId();
//////
////                } else {
////                    // Record not found.
////                    Toast.makeText(getActivity(), "Note not found.", Toast.LENGTH_SHORT).show();
////                    getActivity().finish();
////                }
////            }
////        });
//
//
//
//
//
//    }
//
//
//
//
//
//    @OnClick(R.id.button_preview_File)
//    protected void startpreviewActivity() {
//
//                        for (int i = 0; i < NewUriList.size(); i++) {
//
//                            if (NewUriList.get(i).isSelect()) {//if  select then delete
//                                Uri uri = Uri.parse(NewUriList.get(i).uri);
//
//                                try {
//                                    DocumentsContract.renameDocument(getContext().getContentResolver(), uri, NewUriList.get(i).name);
////                                    handle.sendMessage(handle.obtainMessage());
//                                } catch (FileNotFoundException e) {
//                                    e.printStackTrace();
//                                }
//                            }
////                        }
////
////                        if (progressDialog.getProgress() == progressDialog.getMax()) {
////                            progressDialog.dismiss();
////                        }
////                    }
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
//           }
////        }).start();
//        startActivity(new Intent(getActivity(), FileActivity.class));
//    }
//
//
//
//    private void EditFile() {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle(getResources().getString(R.string.menu_rename));
//        final EditText input = new EditText(getActivity());
//        builder.setView(input);
//
//        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                m_Text = input.getText().toString();
//                MyUriViewModel myUriViewModel = new MyUriViewModel(getActivity().getApplication());
//
//                MyUri myUri = new MyUri();
//                myUri.setName(m_Text);
//
//
//                if (checkspecialcharater(m_Text)) {
//                    update(m_Text);
////                    myUriViewModel.rename(myUri.getName());
//                } else {
//                    Toast.makeText(getActivity(),getResources().getString(R.string.invalidinput), Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });
//        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//
//        builder.show();
//    }
//
//    private void InsertCharAt() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle(getResources().getString(R.string.menu_inserchar));
//        final EditText input = new EditText(getActivity());
//        builder.setView(input);
//
//        String[] charattype = {getResources().getString(R.string.radio_first), getResources().getString(R.string.radio_last)};
//
//        int checkedItem = 1;
//        builder.setSingleChoiceItems(charattype, checkedItem, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                switch (which) {
//                    case 0:
//                        option = 0;
//                    case 1:
//                        option = 1;
//                }
//            }
//        });
//        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                m_Text = input.getText().toString();
//
//                if (checkcharater(m_Text)) {
//                    updatechar(option, m_Text);
//                } else {
//                    Toast.makeText(getActivity(),getResources().getString(R.string.invalidinput), Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//
//        builder.show();
//
//
//    }
//
//    private void ReplaceChar() {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle(getResources().getString(R.string.menu_replacechar));
//        LinearLayout layout = new LinearLayout(getActivity());
//        layout.setOrientation(LinearLayout.VERTICAL);
//
//        final EditText input = new EditText(getActivity());
//        input.setHint(getResources().getString(R.string.oldchar));
//        layout.addView(input);
//
//        final EditText newinput = new EditText(getActivity());
//        newinput.setHint(getResources().getString(R.string.newchar));
//        layout.addView(newinput);
//
//        builder.setView(layout);
//
//        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                old_str = input.getText().toString();
//                new_str = newinput.getText().toString();
//
//                if (checkcharater(old_str) && checkcharater(new_str)) {
//                    update_replacechar(old_str, new_str);
//                } else {
//                    Toast.makeText(getActivity(),getResources().getString(R.string.invalidinput), Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });
//        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//        builder.show();
//    }
//
//
//    private void InsertNumber() {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle(getResources().getString(R.string.menu_insertnumbeer));
//        final EditText input = new EditText(getActivity());
//        builder.setView(input);
//        String[] charattype = {getResources().getString(R.string.radio_first), getResources().getString(R.string.radio_last)};
//
//        int checkedItem = 1;
//        builder.setSingleChoiceItems(charattype, checkedItem, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                switch (which) {
//                    case 0:
//                        option = 0;
//                    case 1:
//                        option = 1;
//                }
//            }
//        });
//
//        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                m_Text = input.getText().toString();
//
//                if (validate(m_Text)) {
//                    update_insertNumber(option, m_Text);
//                } else {
//                    Toast.makeText(getActivity(),getResources().getString(R.string.invalidinput), Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });
//        builder.setNegativeButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//
//        builder.show();
//
//    }
//
//    private void InsertFileSize() {
//
//        ArrayList<MyUri> temp = new ArrayList<>();
//        int id = 0;
//
//        for (int i = 0; i < MyUriList.size(); i++) {
//
//
//            if (MyUriList.get(i).isSelect()) {
//                Uri uri = Uri.parse(MyUriList.get(i).uri);
//
//                String fileSize = null;
//                Cursor cursor = getContext().getContentResolver()
//                        .query(uri, null, null, null, null, null);
//                try {
//                    if (cursor != null && cursor.moveToFirst()) {
//
//                        // get file size
//                        int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
//                        if (!cursor.isNull(sizeIndex)) {
//                            fileSize = cursor.getString(sizeIndex);
//                        }
//                    }
//                } finally {
//                    cursor.close();
//                }
//                int checkfile = Integer.parseInt(fileSize);
//
//                if (checkfile < 1024) {
//                    fileSize = String.valueOf(checkfile).concat("Byte");
//                } else if (checkfile > 1024 && checkfile < (1024 * 1024)) {
//                    fileSize = String.valueOf(Math.round((checkfile / 1024 * 100.0)) / 100.0).concat("KB");
//                } else {
//                    fileSize = String.valueOf(Math.round((checkfile / (1024 * 1204) * 100.0)) / 100.0).concat("MB");
//                }
//
//                String replaceString = MyUriList.get(i).name.replaceAll("\\s*\\([^\\)]*\\)\\s*", " ");
//                replaceString = replaceString + fileSize + "(" + (i) + ")";
//
//                MyUriList.get(i).name = replaceString;
//                temp.add(MyUriList.get(i));
//                NewUriList = temp;
//            }
//            mAdapter.notifyDataSetChanged();
//        }
//
//
//    }
//
//    private void update_insertNumber(int option, String m_text) {
//
//
//        int insernumber = Integer.parseInt(m_Text);
//        ArrayList<MyUri> temp = new ArrayList<>();
//        int id = 0;
//
//
//        for (int i = 0; i < MyUriList.size(); i++) {
//
//            if (MyUriList.get(i).isSelect()) {
//                Uri uri = Uri.parse(MyUriList.get(i).uri);
//                String replaceString = MyUriList.get(i).name.replaceAll("\\s*\\([^\\)]*\\)\\s*", "");
//
//                switch (option) {
//                    case 1:
//                        MyUriList.get(i).name = insernumber + replaceString + "(" + (i) + ")";
//                        break;
//                    case 0:
//                        MyUriList.get(i).name = replaceString + insernumber + "(" + (i) + ")";
//                        break;
//
//                }
//                temp.add(MyUriList.get(i));
//                NewUriList = temp;
//            }
//            mAdapter.notifyDataSetChanged();
//        }
//    }
//
//    private void update_replacechar(String old_str, String new_str) {
//        Uri new_uri = null;
//        ArrayList<MyUri> temp = new ArrayList<>();
//        int id = 0;
//        String newname = "";
//        for (int i = 0; i < MyUriList.size(); i++) {
//
//            if (MyUriList.get(i).isSelect()) {
//                Uri uri = Uri.parse(MyUriList.get(i).uri);
//                String replacename = MyUriList.get(i).name.replaceAll("\\s*\\([^\\)]*\\)\\s*", " ");
//                newname = replacename.replace(old_str, new_str);
//
//                MyUriList.get(i).name = newname + "(" + (i) + ")";
//                temp.add(MyUriList.get(i));
//                NewUriList = temp;
//
//
//            }
//            mAdapter.notifyDataSetChanged();
//        }
//    }
//
//    private void updatechar(int option, String m_text) {
//        Uri new_uri = null;
//        ArrayList<MyUri> temp = new ArrayList<>();
//        int id = 0;
//
//        for (int i = 0; i < MyUriList.size(); i++) {
//
//            if (MyUriList.get(i).isSelect()) {
//                Uri uri = Uri.parse(MyUriList.get(i).uri);
//
//                String replaceString = MyUriList.get(i).name.replaceAll("\\s*\\([^\\)]*\\)\\s*", "");
//
//                switch (option) {
//                    case 1:
//                        MyUriList.get(i).name = m_Text + replaceString + "(" + (i) + ")";
//                        break;
//                    case 0:
//                        MyUriList.get(i).name = replaceString + m_Text + "(" + (i) + ")";
//                        break;
//
//                }
//                temp.add(MyUriList.get(i));
//                NewUriList = temp;
//            }
//            mAdapter.notifyDataSetChanged();
//        }
//    }
//
//    private void update(String m_Text) {
//        Uri new_uri = null;
//
//        ArrayList<MyUri> temp = new ArrayList<>();
//        int id = 0;
//
//        for (int i = 0; i < MyUriList.size(); i++) {
//
//            if (MyUriList.get(i).isSelect()) {
//                Uri uri = Uri.parse(MyUriList.get(i).uri);
//                String replaceString = MyUriList.get(i).name.replaceAll("\\s*\\([^\\)]*\\)\\s*", " ");
//                replaceString = m_Text + "(" + (i) + ")";
//                MyUriList.get(i).name = replaceString;
//                temp.add(MyUriList.get(i));
//                NewUriList = temp;
//            }
//            mAdapter.notifyDataSetChanged();
//        }
//    }
//
//
//
//
//    public String getFileName(Uri uri) {
//        String result = null;
//        if (uri.getScheme().equals("content")) {
//            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
//            try {
//                if (cursor != null && cursor.moveToFirst()) {
//                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
//                }
//            } finally {
//                cursor.close();
//            }
//        }
//        if (result == null) {
//            result = uri.getPath();
//            int cut = result.lastIndexOf('/');
//            if (cut != -1) {
//                result = result.substring(cut + 1);
//            }
//        }
//        return result;
//    }
//
//    private boolean validate(String mValue) {
//
//        try {
//            Integer.parseInt(mValue);
//            return true;
//        } catch (NumberFormatException ex) {
//            return false;
//        }
//    }
//
//    private boolean checkcharater(String mValue) {
//
//        String s = mValue;
//        if (Pattern.matches("[a-zA-Z]+", mValue)) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    private boolean checkspecialcharater(String mValue) {
//
//        String s = mValue;
//        if (Pattern.matches("[a-zA-Z0-9]+", mValue)) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    @Override
//    public void onSaveInstanceState(@NonNull Bundle outState) {
//        outState.putParcelableArrayList(LIST_STATE, MyUriList);
//        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, mRecyclerView.getLayoutManager().onSaveInstanceState());
//        super.onSaveInstanceState(outState);
//    }
//
//    @Override
//    public void onListItemClicked(UriAdapter.DataHolder data) {
//
//    }
//
//    @Override
//    public void onDeleteButtonClicked(UriAdapter.DataHolder data) {
//
//    }
//}
