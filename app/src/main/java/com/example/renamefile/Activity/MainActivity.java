package com.example.renamefile.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.example.renamefile.Fragment.FileFragment;
import com.example.renamefile.Fragment.MainFragment;
//import com.example.renamefile.Fragment.RenameFragment;
import com.example.renamefile.R;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        // Check user is logged in.

        // User is logged in.
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(MainFragment.TAG) == null) {
            // Init fragment.
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, MainFragment.newInstance(), MainFragment.TAG)
                    .commit();
        }


    }
}
