package com.example.renamefile.Activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.renamefile.Fragment.FileFragment;
import com.example.renamefile.Fragment.MainFragment;
//import com.example.renamefile.Fragment.RenameFragment;
import com.example.renamefile.R;

import butterknife.ButterKnife;

public class FileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_file);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(FileFragment.TAG) == null) {
            // Init fragment.
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, FileFragment.newInstance(), FileFragment.TAG)
                    .commit();

        }

    }

}
