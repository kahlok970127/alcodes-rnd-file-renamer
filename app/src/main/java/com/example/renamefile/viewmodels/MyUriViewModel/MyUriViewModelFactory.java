package com.example.renamefile.viewmodels.MyUriViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class MyUriViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;

    public MyUriViewModelFactory(Application application) {
        mApplication = application;
    }


    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MyUriViewModel(mApplication);
    }
}
