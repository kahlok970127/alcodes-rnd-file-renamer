package com.example.renamefile.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.renamefile.Activity.FileActivity;
import com.example.renamefile.Activity.MainActivity;
//import com.example.renamefile.Activity.RenameActivity;
import com.example.renamefile.Adapter.UriAdapter;
import com.example.renamefile.Model.MyUri;
import com.example.renamefile.R;
import com.example.renamefile.database.entities.MyFile;
import com.example.renamefile.utility.DatabaseHelper;
import com.example.renamefile.viewmodels.MyUriViewModel.MyUriViewModel;
import com.example.renamefile.viewmodels.MyUriViewModel.MyUriViewModelFactory;
import com.google.android.material.button.MaterialButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;
import static android.media.CamcorderProfile.get;

public class FileFragment extends Fragment implements UriAdapter.Callbacks {

    public static final String TAG = com.example.renamefile.Fragment.FileFragment.class.getSimpleName();

    @BindView(R.id.uri_recyclerview)
    protected RecyclerView mRecyclerView;
    private Parcelable savedRecyclerLayoutState;
    private static String LIST_STATE = "list_state";
    private static final String BUNDLE_RECYCLER_LAYOUT = "recycler_layout";
    public ArrayList<MyUri> MyUriList = new ArrayList<>();
    public ArrayList<MyUri> RenameList = new ArrayList<>();
    private UriAdapter mAdapter;
    private String m_Text = "";
    private final String KEY_RECYCLER_STATE = "recycler_state";
    public int i = 0;
    private MyUriViewModel mViewModel;
    public Long dataid = 0L;
    public String dataname = "";
    public int option;
    public String old_str = "";
    public String new_str = "";
    private Unbinder mUnbinder;

    public FileFragment() {
    }

    public static com.example.renamefile.Fragment.FileFragment newInstance() {
        return new com.example.renamefile.Fragment.FileFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_file, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        initView();
        initViewModel();
    }

    @Override
    public void onDeleteButtonClicked(UriAdapter.DataHolder data) {
        mViewModel.deleteFile(data.id);
    }


    private void initView() {
        mAdapter = new UriAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new MyUriViewModelFactory(getActivity().getApplication())).get(MyUriViewModel.class);
        mViewModel.getMyUriAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<UriAdapter.DataHolder>>() {

            @Override
            public void onChanged(List<UriAdapter.DataHolder> dataHolders) {
                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();

            }
        });
        mViewModel.loadMyUriAdapterList();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_file, menu);
        inflater.inflate(R.menu.menu_fragment_edit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_add) {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            startActivityForResult(intent, 200);
        } else if (itemId == R.id.menu_edit) {
            EditFile();
        } else if (itemId == R.id.menu_insertchar) {
            InsertCharAt();
        } else if (itemId == R.id.menu_replace) {
            ReplaceChar();
        } else if (itemId == R.id.menu_insertnumber) {
            InsertNumber();
        } else if (itemId == R.id.menu_insertfilesize) {
            InsertFileSize();
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 200 && resultCode == Activity.RESULT_OK && data != null) {
            Uri uri = data.getData();

            DocumentFile documentFile = DocumentFile.fromTreeUri(getActivity(), uri);

            DocumentFile[] files = documentFile.listFiles();

            if (files != null && files.length > 0) {
                for (DocumentFile file : files) {
//
                    mViewModel.addFile(getFileName(file.getUri()), file.getUri().toString());

                }
            }

        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    @Override
    public void onListItemClicked(UriAdapter.DataHolder data) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.menu_rename));
        final EditText input = new EditText(getActivity());
        builder.setView(input);

        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();
                MyUriViewModel myUriViewModel = new MyUriViewModel(getActivity().getApplication());

                MyFile myFile = new MyFile();
                myFile.setId(data.id);
                myFile.setName(m_Text);

                if (checkspecialcharater(m_Text)) {
//                    update(m_Text);
                    myUriViewModel.editsingle(myFile.getId(), myFile.getName());
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.invalidinput), Toast.LENGTH_SHORT).show();
                }

            }
        });
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();


    }

    private void EditFile() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.menu_rename));
        final EditText input = new EditText(getActivity());
        builder.setView(input);

        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();
                int i = 0;
                if (checkspecialcharater(m_Text)) {

                    for (UriAdapter.DataHolder dataHolder : mViewModel.getMyUriAdapterListLiveData().getValue()) {

                        dataid = dataHolder.id;


                        MyUriViewModel myUriViewModel = new MyUriViewModel(getActivity().getApplication());
                        MyFile myFile = new MyFile();
                        myFile.setName(m_Text + "(" + i + ")");
                        myFile.setId(dataid);
                        i++;
                        myUriViewModel.editsingle(myFile.getId(), myFile.getName());

                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.invalidinput), Toast.LENGTH_SHORT).show();
                }
//                    i++;
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void InsertCharAt() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.menu_inserchar));
        final EditText input = new EditText(getActivity());
        builder.setView(input);

        String[] charattype = {getResources().getString(R.string.radio_first), getResources().getString(R.string.radio_last)};

        int checkedItem = 1;
        builder.setSingleChoiceItems(charattype, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        option = 0;
                    case 1:
                        option = 1;
                }
            }
        });
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();
                int i = 0;
                if (checkcharater(m_Text)) {
                    for (UriAdapter.DataHolder dataHolder : mViewModel.getMyUriAdapterListLiveData().getValue()) {


                        dataid = dataHolder.id;
                        dataname = dataHolder.name;
                        String replaceString = dataname.replaceAll("\\s*\\([^\\)]*\\)\\s*", "");

                        switch (option) {
                            case 1:
                                dataname = m_Text + replaceString + "(" + (i) + ")";
                                break;
                            case 0:
                                dataname = replaceString + m_Text + "(" + (i) + ")";
                                break;
                        }

                        MyUriViewModel myUriViewModel = new MyUriViewModel(getActivity().getApplication());
                        MyFile myFile = new MyFile();
                        myFile.setName(dataname);
                        i++;
                        myFile.setId(dataid);

                        myUriViewModel.editsingle(myFile.getId(), myFile.getName());

                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.invalidinput), Toast.LENGTH_SHORT).show();
                }


            }


        });
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void InsertNumber() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.menu_inserchar));
        final EditText input = new EditText(getActivity());
        builder.setView(input);

        String[] charattype = {getResources().getString(R.string.radio_first), getResources().getString(R.string.radio_last)};

        int checkedItem = 1;
        builder.setSingleChoiceItems(charattype, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        option = 0;
                    case 1:
                        option = 1;
                }
            }
        });
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();
                int i = 0;
                if (validate(m_Text)) {
                    for (UriAdapter.DataHolder dataHolder : mViewModel.getMyUriAdapterListLiveData().getValue()) {


                        dataid = dataHolder.id;
                        dataname = dataHolder.name;
                        String replaceString = dataname.replaceAll("\\s*\\([^\\)]*\\)\\s*", "");

                        switch (option) {
                            case 1:
                                dataname = m_Text + replaceString + "(" + (i) + ")";

                                break;
                            case 0:
                                dataname = replaceString + m_Text + "(" + (i) + ")";

                                break;

                        }

                        MyUriViewModel myUriViewModel = new MyUriViewModel(getActivity().getApplication());
                        MyFile myFile = new MyFile();
                        myFile.setName(dataname);
                        i++;
                        myFile.setId(dataid);


                        myUriViewModel.editsingle(myFile.getId(), myFile.getName());

                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.invalidinput), Toast.LENGTH_SHORT).show();
                }


            }


        });
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


    private void ReplaceChar() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.menu_replacechar));
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText input = new EditText(getActivity());
        input.setHint(getResources().getString(R.string.oldchar));
        layout.addView(input);

        final EditText newinput = new EditText(getActivity());
        newinput.setHint(getResources().getString(R.string.newchar));
        layout.addView(newinput);

        builder.setView(layout);

        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                old_str = input.getText().toString();
                new_str = newinput.getText().toString();
                int i = 0;
                if (checkcharater(old_str) && checkcharater(new_str)) {

                    for (UriAdapter.DataHolder dataHolder : mViewModel.getMyUriAdapterListLiveData().getValue()) {


                        dataid = dataHolder.id;
                        dataname = dataHolder.name;
                        i++;
                        String replaceString = dataname.replaceAll("\\s*\\([^\\)]*\\)\\s*", "");
                        dataname = replaceString.replace(old_str, new_str);

                        MyUriViewModel myUriViewModel = new MyUriViewModel(getActivity().getApplication());
                        MyFile myFile = new MyFile();
                        myFile.setName(dataname + "(" + (i) + ")");
                        myFile.setId(dataid);


                        myUriViewModel.editsingle(myFile.getId(), myFile.getName());
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.invalidinput), Toast.LENGTH_SHORT).show();
                }

            }
        });
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void InsertFileSize() {
        int i = 0;
        for (UriAdapter.DataHolder dataHolder : mViewModel.getMyUriAdapterListLiveData().getValue()) {

            Uri uri = Uri.parse(dataHolder.uri);

            String fileSize = null;
            Cursor cursor = getContext().getContentResolver()
                    .query(uri, null, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {

                    // get file size
                    int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                    if (!cursor.isNull(sizeIndex)) {
                        fileSize = cursor.getString(sizeIndex);
                    }
                }
            } finally {
                cursor.close();
            }
            int checkfile = Integer.parseInt(fileSize);

            if (checkfile < 1024) {
                fileSize = String.valueOf(checkfile).concat("Byte");
            } else if (checkfile > 1024 && checkfile < (1024 * 1024)) {
                fileSize = String.valueOf(Math.round((checkfile / 1024 * 100.0)) / 100.0).concat("KB");
            } else {
                fileSize = String.valueOf(Math.round((checkfile / (1024 * 1204) * 100.0)) / 100.0).concat("MB");
            }

            dataid = dataHolder.id;
            dataname = dataHolder.name;
            i++;
            String replaceString = dataname.replaceAll("\\s*\\([^\\)]*\\)\\s*", "");

            dataname = replaceString + "_" + fileSize + "(" + (i) + ")";

            MyUriViewModel myUriViewModel = new MyUriViewModel(getActivity().getApplication());
            MyFile myFile = new MyFile();
            myFile.setName(dataname);
            myFile.setId(dataid);


            myUriViewModel.editsingle(myFile.getId(), myFile.getName());
        }

    }

    @OnClick(R.id.button_rename_File)
    protected void startGetFileActivity() {
        for (UriAdapter.DataHolder dataHolder : mViewModel.getMyUriAdapterListLiveData().getValue()) {
            Uri uri = Uri.parse(dataHolder.uri);
            dataname = dataHolder.name;
            dataid = dataHolder.id;

            try {
                DocumentsContract.renameDocument(getContext().getContentResolver(), uri, dataname);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            mViewModel.deleteFile(dataid);
        }

        startActivity(new Intent(getActivity(), MainActivity.class));
    }

    private boolean checkspecialcharater(String mValue) {

        String s = mValue;
        if (Pattern.matches("[a-zA-Z0-9]+", mValue)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean validate(String mValue) {

        try {
            Integer.parseInt(mValue);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    private boolean checkcharater(String mValue) {

        String s = mValue;
        if (Pattern.matches("[a-zA-Z]+", mValue)) {
            return true;
        } else {
            return false;
        }
    }

}