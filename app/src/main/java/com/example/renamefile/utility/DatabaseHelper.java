package com.example.renamefile.utility;

import android.content.Context;

import com.example.renamefile.database.DbOpenHelper;
import com.example.renamefile.database.entities.DaoMaster;
import com.example.renamefile.database.entities.DaoSession;

public class DatabaseHelper {
    private static DaoSession mInstance;

    public static DaoSession getInstance(Context context) {
        if (mInstance == null) {
            synchronized (DatabaseHelper.class) {
                if (mInstance == null) {
                    DbOpenHelper dbOpenHelper = new DbOpenHelper(context, "app");

                    mInstance = new DaoMaster(dbOpenHelper.getWritableDb()).newSession();
                }
            }
        }

        return mInstance;
    }

    private DatabaseHelper() {
    }
}
