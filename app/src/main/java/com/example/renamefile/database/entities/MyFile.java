package com.example.renamefile.database.entities;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import org.greenrobot.greendao.annotation.NotNull;

@Entity
public class MyFile {

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String uri;

    @Generated(hash = 1867234944)
    public MyFile(Long id, @NotNull String name, @NotNull String uri) {
        this.id = id;
        this.name = name;
        this.uri = uri;
    }

    @Generated(hash = 1178448338)
    public MyFile() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }


}